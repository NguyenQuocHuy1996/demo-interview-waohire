$(document).ready(function () {
  $('#WhatRealPeopleSay').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    asNavFor: '#WhatRealPeopleSay-Mob',
    prevArrow: `<span class="slick-prev"><img src="./assets/img/1-Home/next.svg" alt="" class="ml-1" style="transform: rotate(-180deg);" /></span>`,
    nextArrow: `<span class="slick-next"><img src="./assets/img/1-Home/next.svg" alt="" class="ml-1" /></span>`
  });
  $('#WhatRealPeopleSay-Mob').slick({
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    asNavFor: '#WhatRealPeopleSay',
    variableWidth: true
  });
});